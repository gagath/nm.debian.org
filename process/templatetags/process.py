from django import template

register = template.Library()


@register.filter
def process_status(process, view):
    from process.mixins import compute_process_status
    perms = getattr(view, "visit_perms", None)
    return compute_process_status(process, view.request.user, perms)
