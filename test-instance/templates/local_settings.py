from .settings import TEMPLATES
import os.path

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.join(PROJECT_DIR, '../data')

# Avoid sending emails
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = '/tmp/nm_notification_mails'

SECRET_KEY = '{{None|django_secret_key}}'

DEBUG = False

STATIC_ROOT = '/srv/nm.debian.org/htdocs/static/'

ALLOWED_HOSTS = [
    "nm-test.debian.net",
    "18.156.100.54",
]


TEMPLATES[0]["DIRS"].append("/srv/nm.debian.org/nm2/templates/")
STATIC_ROOT = '/srv/nm.debian.org/htdocs/static/'
