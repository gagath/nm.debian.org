from __future__ import annotations
from unittest import mock
from django.test import TestCase
from django.urls import reverse
from backend.unittest import BaseFixtureMixin
from backend import const


class TestSalsaExport(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        p = cls.create_person("dc", status=const.STATUS_DC, alioth=True)
        cls.identities.create(
                "dc", person=p, issuer="salsa", subject="1", username="dc", audit_skip=True)

        p = cls.create_person("dd", status=const.STATUS_DD_U)
        cls.identities.create(
                "dd", person=p, issuer="salsa", subject="2", username="dd", audit_skip=True)

        cls.create_person("fd", status=const.STATUS_DD_U, is_staff=True)

    def test_anonymous(self):
        client = self.make_test_client(None)
        with self.assertLogs() as log:
            self.assertGetForbidden(client)
        self.assertEqual(log.output, [
            'WARNING:restricted.views:Remote address 127.0.0.1 does not match []',
        ])

    def test_anonymous_whitelisted_ip(self):
        with self.settings(SALSA_EXPORT_ALLOW_IPS=["127.0.0.1"]):
            client = self.make_test_client(None)
            self.assertGetAllowed(client)

    def test_dd(self):
        client = self.make_test_client(self.persons.dd)
        with self.assertLogs() as log:
            self.assertGetForbidden(client)
        self.assertEqual(log.output, [
            'WARNING:restricted.views:Remote address 127.0.0.1 does not match []',
        ])

    def test_fd(self):
        client = self.make_test_client(self.persons.fd)
        self.assertGetAllowed(client)

    def assertGetForbidden(self, client):
        response = client.get(reverse("export_salsa"))
        self.assertPermissionDenied(response)

    def assertGetAllowed(self, client):
        with mock.patch("restricted.views.time.time", return_value=100):
            response = client.get(reverse("export_salsa"))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'exp': 259300,
            'https://nm.debian.org/claims/persons': [
                {
                    'aud': 'https://salsa-test.debian.net',
                    'email': 'dd@debian.org',
                    'https://nm.debian.org/claims/debian_status': 'debian_developer',
                    'profile': 'http://testserver/person/dd/',
                    'sub': '2',
                }
            ],
            'iss': 'https://example.com',
        })
