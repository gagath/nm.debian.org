# Syncing badges on Salsa

If you go to https://salsa.debian.org/enrico you see at the top right a "Debian
Developer" badge that links to Enrico's nm.d.o page.

Salsa syncs with nm.debian.org using `restricted.views.SalsaExport`.

IIRC we did an initial sync while the Salsa accounts were still aligned with LDAP, 
and nm.debian.org has salsa information for everyone who was in LDAP at that point.

Since nm.debian.org is (almost) a gatekeeper for LDAP, the information should
still be valid. "Almost" means that DSA can still create LDAP accounts without
passing through nm.debian.org, and nm.debian.org would not know anything about
them, but that happens rarely if at all.

A possibly annoying corner case is if someone goes through NM using the
cert-based SSO and not Salsa, then they very likely won't show up as Debian
Developers on Salsa. Logging into nm.d.o with both accounts will establish the
connection and likely generate the badge the time Salsa fetches nm.d.o
information.
